// [SECTION] JS Server
const express = require("express");
// mongoose is a package/module that allows the creation of schemas to model our data structures and aslo has access to different moethods for manipulating our database
const mongoose = require("mongoose");

const app = express();
const port = 3000;

// [SECTION] MongoDB Connection
// {newUrlParses: true} allows us to avoid any current and future errors while connecting to MongoDB
/*
	Syntax
		mongoose.connect("<MongoDB Atlas connection string>", {useNewUrlParser: true});
*/

mongoose.connect("mongodb+srv://admin:admin1234@b256buencido.lmfwv9e.mongodb.net/B256_to-do?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true 
});

// Checking of connection 
// Allows to handle errors when the initial connection is established
// Works with the on and once Mongoose Methods
let db = mongoose.connection;

// If a connection error occurred, output in the console
// console.error.bind(console) allows us to print errors in the browser console and in the terminal
// "connection error" is the message that will display if an error is encountered
db.on("error", console.error.bind(console, "connection error")); 

// If the connection is successful, output in the console
db.once("open", () => console.log(`Were connected to the cloud database`));

// [SECTION] Mongoose Schema
// Schemas determine the structure of the document to be written in the database
// in simple terms, it acts like a blueprint of our data
// Use the Schema() constructor of the Mongoose module to create a new Schema object
// The "new" keyword creates a new Schema
const taskSchema = new mongoose.Schema({
	// Define the field with the corresponding data type
	// For a task, it needs a "task name" and "task status"
	// There is a field called "name" and its data type is "String"
	name: String,
	// There is a field called "status" that is a "String" and the default value is "pending"
	status: {
		type: String,
		// Default values are the predefined values for a field if we don't put any value.
		default: "pending"
	}
});

// Create a User Schema
const userSchema = new mongoose.Schema({

	username: String,
	password: String
});

// [SECTION] Models
// Uses schemas and are used to create/instantiate objects that correspond to the schema
// Models use Schemas and they act as a middleman from the server (JS code) to our database
//  Server > Schema (blueprint) > Database > Collection

// Models must be in singular form and capitalized
// The first parameter of the Mongoose model method indicates the collection in where to store the data
// The second parameter is used to specify the Schema/blueprint of the documents that will be stored in the MongoDB collection
// Using Mongoose, the package was programmed well enough that it automatically converts the singular form of the model name into a plural form when creating a collection in postman
const Task = mongoose.model("Task", taskSchema);

// Create a User model
const User = mongoose.model("User", userSchema);

// Middlewares
app.use(express.json());
// The URL, by default, accepts only string and array data types. By extending the urlencoded, the app can now accept object data type as well.
app.use(express.urlencoded({extended: true})); // read forms

// Creating a new Task
/*
	Business Logic:
		1. Add a functionality to check if there are duplicate tasks
			- If the task already exists in the database, we return an error
			- If the task doesn't exist in the database, we add it in the database
		2. The task data will be coming from the request's body
		3. Create a new Task object with a "name" field/property
		4. The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object
*/
app.post("/task", (request, response) => {

	// "findOne" is a Mongoose method that acts similar to "find" of MongoDB
	// findOne() returns the first document that matches the search criteria as a single object
	Task.findOne({name: request.body.name}).then((result, err) => {

		if(result !== null && result.name == request.body.name) {

			return response.send("Duplicate Task Found");
		}
		else {
			let newTask = new Task ({
				name: request.body.name
			});

			newTask.save().then((savedTask, savedErr) => {
				if (savedErr) {

					return console.error(savedErr);
				}
				else {
					return response.status(201).send("New Task Created");
				}
			})
		}

	})
})

// [SECTION] Getting All Tasks
// Business Logic
/*
	1. Retrieve all the documents
	2. If an error is encountered, print the error
	3. If no errors are found, send a success status back to the client/Postman and return an array of documents
*/
app.get("/tasks", (request, response) => {

	Task.find({}).then((result, err) => {

		if (err) {

			return console.error(err)
		}
		else {

			response.status(200).json({
				data: result
			})
		}
	})
})

// Activity
// Create a POST route that will access the /signup route that will create a user.
app.post("/signup", (request, response) => {

	User.findOne({username: request.body.username}).then(result => {

		if (result != null && result.username == request.body.username) {

			return response.send("Username already used.");
		}
		else{

			if (request.body.username != "" && request.body.password != "") {

				let newUser = new User(
				{
					username: request.body.username,
					password: request.body.password
				}
				);

				newUser.save().then((savedUser, err) => {
					if (err) {
						return console.log(err)
					}
					else{
						return response.status(200).send("Registration Success!")
					}
				})
			}
			else{
				return response.send("Please provide username and password details")
			};
		};
	}).catch(error => response.send(error))
});

app.listen(port, () => console.log(`Server is running at port: ${port}`));

